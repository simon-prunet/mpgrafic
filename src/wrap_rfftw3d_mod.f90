!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   wrap_rfftw3d_mod - fortran wrapper for bits of fftw2                  !
!                                                                         !
!   Copyright (C) 2017-2019 by Boud Roukema                               !
!   boud cosmo.torun.pl                                                   !
!                                                                         !
!   This program is free software; you can redistribute it and/or modify  !
!   it under the terms of the GNU General Public License as published by  !
!   the Free Software Foundation; either version 2 of the License, or     !
!   (at your option) any later version.                                   !
!                                                                         !
!   This program is distributed in the hope that it will be useful,       !
!   but WITHOUT ANY WARRANTY; without even the implied warranty of        !
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         !
!   GNU General Public License for more details.                          !
!                                                                         !
!   You should have received a copy of the GNU General Public License     !
!   along with this program; if not, write to the                         !
!   Free Software Foundation, Inc.,                                       !
!   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA              !
!   or see https://www.gnu.org/licenses/licenses.html#GPL .               !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module wrap_rfftw3d_mod
  ! see e.g. https://stackoverflow.com/tags/fortran-iso-c-binding/info

  interface
     subroutine rfftw3d_f77_mpi_create_plan_c_wrap &
          (rfftwnd_mpi_plan, comm, &
          nx, ny, nz, idir, flags) &
          bind(C, name='rfftw3d_f77_mpi_create_plan_c_wrap')
       use iso_C_binding
#ifdef HAVE_USE_MPI_F08
       use mpi_f08
#endif
       implicit none
#ifndef HAVE_USE_MPI_F08
       include 'mpif.h'
#endif
       type (c_ptr) :: rfftwnd_mpi_plan
       /*
       C vs fortran communicator types are the reason
       for this extra wrapper. One implementation of
       the C communicator typedef is the following:

       /usr/lib/openmpi/include/mpi.h
       typedef struct ompi_communicator_t *MPI_Comm

       Here we bypass this, for programs in which
       only the MPI_COMM_WORLD communicator is needed.
       */
#ifdef HAVE_USE_MPI_F08
       type(MPI_Comm) :: comm ! dummy
#else
       integer :: comm ! dummy
#endif
       integer(c_int) :: nx, ny, nz
       integer(c_int) :: idir, flags
     end subroutine rfftw3d_f77_mpi_create_plan_c_wrap
  end interface

end module wrap_rfftw3d_mod
