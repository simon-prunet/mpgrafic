#!/bin/sh
# regression-test-0.3.7.9.sh - check consistency of mpgrafic behaviour versus v0.3.7.9

#/***************************************************************************
# *   Copyright (C) 2017 Boud Roukema                                       *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU General Public License for more details.                          *
# *                                                                         *
# *   You should have received a copy of the GNU General Public License     *
# *   along with this program; if not, write to the                         *
# *   Free Software Foundation, Inc.,                                       *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA              *
# *   or see https://www.gnu.org/licenses/licenses.html#GPL .               *
# **************************************************************************/


printf "Code regression test: does the standard input file generate an\n"
printf "output file identical to that produced by version 0.3.7.9\n"
printf "of mpgrafic? IEEE_UNDERFLOW_FLAG and IEEE_DENORMAL exceptions\n"
printf "are considered acceptable and ignored. Other minor warnings are\n"
printf "ignored too on some systems/implementations.\n\n"

## Run mpgrafic for a tiny number of particles with mpi - the aim is to test
## algorithms and arithmetic, not computer power.

EXPECTED_OUTPUT=${srcdir}/Output.stdout.np32
TMP_OUT=Output.stdout.np32.tmp
TMP_OUT_CLEANED=Output.stdout.np32.tmp.2

## Running openmpi (on Debian) in a chroot jail with a minimal system
## installed will fail if ssh (and rsh, which is not encrypted) are
## absent from the system. Since ssh is not needed for testing on a
## single processor, the default "rsh_agent" can be safely overridden
## by the default shell "sh", but only if the mpirun (e.g. a symbolic link
## to orterun) options follow a known syntax.
##
## If the system is Debian with mpi-default-dev installed, then
## the check can be done as below.
##
## Adding a build dependency on ssh would be unreasonable, since, for
## example, mpi jobs can be run on multi-processor single
## machines. Moreover, it would add an extra burden in terms of
## automatic checking of packages for security issues.
##
## Please add cases for other mpi implementations or GNU/Linux
## distribution systems, or propose a more generic solution.
##
## 2017-08-14: Switch --mca plm_rsh_agent ssh to
## --mca plm_rsh_agent /bin/false - see
##  https://lists.debian.org/debian-science/2015/10/msg00032.html +
##  http://www.open-mpi.de/faq/?category=rsh
## This may enable builds on kfreebsd-(amd64|i386) and does not
## reduce the power of the test: ssh is obviously
## *not* desirable in automated builds of mpgrafic, and is not tested.

##
if [ -r /usr/share/mpi-default-dev/debian_defaults ] ; then
    MPI_IMPL=$(grep ARCH_DEFAULT_MPI_IMPL \
                    /usr/share/mpi-default-dev/debian_defaults | \
                      sed -e 's/ARCH_DEFAULT_MPI_IMPL *= *//' | \
                      tail -n 1) # last evaluation overrides
    if [ "x${MPI_IMPL}" = "xopenmpi" ]; then
        printf "This looks like a debian openmpi system.\n"
        mpirun -n 1 --mca plm_rsh_agent /bin/false \
               ${MPI_ALLOW_RUN_AS_ROOT} \
               ${srcdir}/src/mpgrafic --np=32 ${srcdir}/Input.stdin \
               > ${TMP_OUT}
    else
        printf "Warning: This looks like a debian system, but \n"
        printf "options for ${MPI_IMPL} have not yet been implemented,\n"
        printf "so this test will pass without doing any work.\n"
        exit 0
    fi
else
    printf "MPI implementation unknown.\n"
    mpirun -n 1 ${srcdir}/src/mpgrafic --np=32 ${srcdir}/Input.stdin > ${TMP_OUT}
fi

## Ignore known warnings from non-openmpi implementations of mpi:
cat ${TMP_OUT} | \
    grep -v "Warning: Cannot receive number of cores .*Round robin.*startup scheme will be involved" > \
         ${TMP_OUT_CLEANED}


if ( [ -r ${EXPECTED_OUTPUT} ] && [ -r ${TMP_OUT_CLEANED} ] ); then
    LINES_DIFF=$(diff ${EXPECTED_OUTPUT} ${TMP_OUT_CLEANED} | wc -l)
    if [ "x${LINES_DIFF}" = "x0" ] ; then
        printf "A 32^3 mpi run of mpgrafic matched the expected output.\n"
    else
        printf "Warning: A 32^3 mpi run of mpgrafic did not match the expected output.\n"
        exit 1
    fi
else
    printf "Warning: ${EXPECTED_OUTPUT} or ${TMP_OUT_CLEANED} is not found or not readable.\n"
    exit 1
fi
#rm -f ${TMP_OUT}

exit 0
